(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.imageField360 = {
    attach: function (context) {
      $(".photosphere").each(function (index, element) {
        var container = $(this).get(0);
        var imgSrc = $(this).find(".image-photosphere").attr("src");

        var photosphereSettings = $(this).attr("data-photosphere");
        photosphereSettings = JSON.parse(photosphereSettings);
        photosphereSettings.panorama = imgSrc;
        photosphereSettings.container = container;

        new PhotoSphereViewer(photosphereSettings);
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
