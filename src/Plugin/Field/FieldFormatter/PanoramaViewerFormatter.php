<?php

namespace Drupal\panorama_viewer\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Panorama Viewer Formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "Panorama_Viewer_Formatter",
 *   label = @Translation("360 Panorama Viewer Formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class PanoramaViewerFormatter extends FormatterBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Construct a PanoramaViewerFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition interface.
   * @param array $settings
   *   The settings.
   * @param string $label
   *   The label.
   * @param mixed $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   The third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'loading_msg' => 'Loading...',
      'width' => '100%',
      'height' => '500px',
      'navbar_enable' => 0,
      'navbar_enable' => 0,
      'navbar_backgroundColor' => 'rgba(61, 61, 61, 0.5)',
      'navbar_buttonsColor' => 'rgba(255, 255, 255, 0.7)',
      'navbar_buttonsBackgroundColor' => 'transparent',
      'navbar_activeButtonsBackgroundColor' =>
      'rgba(255, 255, 255, 0.1)',
      'navbar_buttonsHeight' => '20',
      'navbar_autorotateThickness' => '1',
      'navbar_zoomRangeWidth' => '50',
      'navbar_zoomRangeThickness' => '1',
      'navbar_zoomRangeDisk' => '7',
      'navbar_fullscreenRatio' => '4/3',
      'navbar_fullscreenThickness' => '2',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();

    $form['loading_msg'] = [
      '#title' => $this->t('Loading message'),
      '#type' => 'textfield',
      '#default_value' => $settings['loading_msg'],
      '#description' => $this->t('The message being displayed while the image is loading (default to Loading...).'),
    ];

    $form['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'textfield',
      '#default_value' => $settings['width'],
      '#description' => $this->t('The width of viewer.'),
    ];

    $form['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'textfield',
      '#default_value' => $settings['height'],
      '#description' => $this->t('The height of viewer.'),
    ];

    $form['navbar_enable'] = [
      '#title' => $this->t('Show Navbar'),
      '#type' => 'checkbox',
      '#default_value' => $settings['navbar_enable'],
      '#description' => $this->t('Enable or disable the navigation bar.'),
      '#attributes' => [
        'id' => 'enable-navbar',
      ],
    ];

    $form['navbar_backgroundColor'] = [
      '#title' => $this->t('Navigation bar background color'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_backgroundColor'],
      '#description' => $this->t('The navigation bar background color (default to rgba(61, 61, 61, 0.5)).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_buttonsColor'] = [
      '#title' => $this->t('Buttons forground color'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_buttonsColor'],
      '#description' => $this->t('The buttons foreground color (default to rgba(255, 255, 255, 0.7)).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_buttonsBackgroundColor'] = [
      '#title' => $this->t('Button background color'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_buttonsBackgroundColor'],
      '#description' => $this->t('The buttons background color (default to transparent).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_activeButtonsBackgroundColor'] = [
      '#title' => $this->t('Active button background color'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_activeButtonsBackgroundColor'],
      '#description' => $this->t('The buttons background color when they are active (default to rgba(255, 255, 255, 0.1)).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_buttonsHeight'] = [
      '#title' => $this->t('Buttons height'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_buttonsHeight'],
      '#description' => $this->t('Buttons height in pixels (default to 20).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_autorotateThickness'] = [
      '#title' => $this->t('Autorotate icon thickness'),
      '#type' => 'textfield',
      '#default_value' =>
      $settings['navbar_autorotateThickness'],
      '#description' => $this->t('Autorotate icon thickness in pixels (default to 1).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_zoomRangeWidth'] = [
      '#title' => $this->t('Zoom range bar width'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_zoomRangeWidth'],
      '#description' => $this->t('Zoom range width in pixels (default to 50).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_zoomRangeThickness'] = [
      '#title' => $this->t('Zoom range bar thickness'),
      '#type' => 'textfield',
      '#default_value' =>
      $settings['navbar_zoomRangeThickness'],
      '#description' => $this->t('Zoom range thickness in pixels (default to 1).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_zoomRangeDisk'] = [
      '#title' => $this->t('Zoom range disk diameter'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_zoomRangeDisk'],
      '#description' => $this->t('Zoom range disk diameter in pixels (default to 7).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_fullscreenRatio'] = [
      '#title' => $this->t('Fullscreen icon ratio'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_fullscreenRatio'],
      '#description' => $this->t('The fullscreen icon ratio (default to 4/3). Allowed formats are width:height and width/height.'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['navbar_fullscreenThickness'] = [
      '#title' => $this->t('Fullscreen icon thickness'),
      '#type' => 'textfield',
      '#default_value' => $settings['navbar_fullscreenThickness'],
      '#description' => $this->t('The fullscreen icon thickness in pixels (default to 2).'),
      '#states' => [
        'visible' => [
          '#enable-navbar' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    $settings = $this->getSettings();
    $summary[] = $this->t('Width: @width', ['@width' => $settings['width']]);
    $summary[] = $this->t('Height: @height', ['@height' => $settings['height']]);
    $summary[] = $this->t('Loading message: @message', ['@message' => $settings['loading_msg']]);
    $summary[] = $this->t('Show Navbar: @navbar', ['@navbar' => ($settings['navbar_enable'] == 1) ? $navbar = $this->t('Yes') : $this->t('No')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $render = [];
    $settings = $this->getSettings();
    foreach ($items as $delta => $item) {
      // Load the file entity.
      $file = $this->entityTypeManager->getStorage('file')->load($item->getValue()['target_id']);

      // Construct the element settings.
      $element_settings = [
        'size' => [
          'width' => $settings['width'],
          'height' => $settings['height'],
        ],
        'loading_msg' => $settings['loading_msg'],
        'navbar' => ($settings['navbar_enable'] == 1) ? TRUE : FALSE,
        'navbar_style' => [
          'backgroundColor' => $settings['navbar_backgroundColor'],
          'buttonsColor' => $settings['navbar_buttonsColor'],
          'buttonsBackgroundColor' => $settings['navbar_buttonsBackgroundColor'],
          'activeButtonsBackgroundColor' => $settings['navbar_activeButtonsBackgroundColor'],
          'buttonsHeight' => $settings['navbar_buttonsHeight'],
          'autorotateThickness' => $settings['navbar_autorotateThickness'],
          'zoomRangeWidth' => $settings['navbar_zoomRangeWidth'],
          'zoomRangeThickness' => $settings['navbar_zoomRangeThickness'],
          'zoomRangeDisk' => $settings['navbar_zoomRangeDisk'],
          'fullscreenRatio' => $settings['navbar_fullscreenRatio'],
          'fullscreenThickness' => $settings['navbar_fullscreenThickness'],
        ],
      ];

      $render[$delta] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'photosphere',
          'data-photosphere' => json_encode($element_settings),
        ],
        'image' => [
          '#theme' => 'image',
          '#uri' => $file->getFileUri(),
          '#attributes' => [
            'class' => 'image-photosphere',
          ],
          '#attached' => [
            'library' => [
              'panorama_viewer/panorama_viewer',
            ],
          ],
        ],
      ];
    }
    return $render;
  }

}
