= 360° panorama viewer =

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## INTRODUCTION

This will provide a field formatter for image fields,
so that the images uploaded for an image field
that lets users expirence a 360° image.

## REQUIREMENTS

This module dependency on jquery_ui  <https://www.drupal.org/project/jquery_ui>


## INSTALLATION

- Download the module and place in contrib module folder.
- Enable the icm module from the modules page / drush / drupal console.
- You should now see a new field formatter for image fields,
  Ex: under Manage display section of each content types.

## CONFIGURATION

- Visit any image fields display settings, you will be able to find
the 360° panorama viewer formatter, select this one and one can also
select image styles.
Ex: admin/structure/types/manage/<content-type-machine-name>/display
- Have the image field settings "Allowed number of values" to 1 image
- Add content & images to the node and Save.
On node view, 360° panorama viewer will appear for that image field.

Warning: Pictures that are not 360×180 degrees panoramas may look strange.

## MAINTAINERS

- Ghazali Mustapha (g.mustapha) - <https://www.drupal.org/u/gmustapha>
